# public_research
#return-oriented #programming #ROP #linux #mitigation #inline #assembly
#security #memory #stack #buffer #overflow #public #research

>>> original research file: psm.pdf

>>> original journal file: journal.pdf

>>> searching engine use : text_in_pdf, text_in_journal

This research is mainly on ROP attack (return / without return)
with mitigation.

[page 48 - 52]
Where traditional ROP attack is only focusing on %rip/%eip,
but this research will propose the new attack that utilizing
both %rip and %rbp under x86_64 archictecture to cause an
infinite loop and useful in denial of service (DOS), if loop
happens in CPU intensive functions.

[page 64]
The mitigation can kill ROP attack at early stage but also
contains its own weaknesses. Security researchers can freely
use the proposed solution (inline assembly code) to kill the
ROP attack in other fields, or even port to other architectures.

Since this project is sticking to Linux kernel and also
Grsecurity/PaX, will be released under LGPLv2 license,
for more freely use as document, without changing their original
source code.
